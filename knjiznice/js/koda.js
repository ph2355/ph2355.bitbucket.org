// google api key AIzaSyBv8bU2WDwE0wTtaYzl8eNuxBBrGn4xtv0
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var demoPodatki = [{name:"Jaka" , lastName:"Skala", dateOfBirth:"1999-05-05", gender:"male", ehrId:"2f538475-64fc-4f40-8d7b-f6cc3c277f0e"},
                  {name:"John" , lastName:"Doe", dateOfBirth:"1975-02-14", gender:"male", ehrId:"193d3a11-fed7-466a-99ba-83ba3d27d9aa"},
                  {name:"Ata" , lastName:"Starešina", dateOfBirth:"1930-09-29", gender:"male", ehrId:"e78da42c-5985-4a5f-8446-0a096f4513a8"}];
                  
/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)

 */
 function generiranjePodatkov(callback) {
     for (var i = 0; i < 3; i++) {
         $("#preset").val(i+1);
         updateGenerateView();
          callback();
     }
 }
 
function generirajPodatke() {
sessionId = getSessionId();

	var ime = $("#generateName").val();
	var priimek = $("#generateLastName").val();
    var datumRojstva = $("#generateDateOfBirth").val() + "T00:00:00.000Z";
    var spol = $("#izberiSpol").val().toUpperCase();
    var ehrId;
	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0 || spol==0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            gender: spol,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
                          var index = -1;
                          for (var i = 0; i < demoPodatki.length; i++) {
                              if (ime == demoPodatki[i].name && priimek == demoPodatki[i].lastName && datumRojstva == (demoPodatki[i].dateOfBirth + "T00:00:00.000Z")){
                                  index = i;
                                  break;
                              }
                          }
                          if(index > -1) demoPodatki[index].ehrId = ehrId;
                          else {
                              kreirajPacienta(ime, priimek, datumRojstva.split("T00:00:00.000Z")[0], spol, ehrId);
                              $("#izbiraOsebe").append("<option value=\"" + demoPodatki.length + "\">" + ime + " " + priimek +"</option>");
                          }
                          
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}

function getData(poizvedba, callback) {
    sessionId = getSessionId();
    var heights = [];
    var weights = [];
    var party;
    
        $.ajax({
        url: baseUrl + "/demographics/ehr/" + poizvedba + "/party",
        type: 'GET',
        headers: {
            "Ehr-Session": sessionId
        },
        success: function (data) {
        party = data.party; 
        callback(weights, heights, party);
        }, error: function(err) {
		    	console.log("napaka pri iskanju osnovnih podatkov");
		    }});
        
        $.ajax({
            
        url: baseUrl + "/view/" + poizvedba + "/weight",
        type: 'GET',
        headers: {"Ehr-Session": sessionId},
        success: function (res) {
            weights = res;
        },
        error: function(err) {
		    	console.log("napaka pri iskanju teže");
		    }
    });
    $.ajax({
        url: baseUrl + "/view/" + poizvedba + "/height",
        type: 'GET',
        headers: {"Ehr-Session": sessionId},
        success: function (res) {
            heights = res;
            
        },
        error: function(err) {
            console.log("napaka pri iskanju višine");
		    }
    });
    
}


function drawTable(weights, heights, gender, age) {
    $("#tabela").children().remove();
    if(weights.length > 0 && heights.length > 0) {
       $("#tabela").append("<table class=\"table table-hover\"> <thead><tr><th>Datum</th> <th>Teža (kg)</th><th>Višina (cm)</th><th>BMI</th><th>Stanje</th></tr></thead> <tbody id=\"tBody\">");
        for (var i = 0; i < heights.length; i++) {
            var time = heights[i].time.split("T")[1].split("Z")[0];
            var date = heights[i].time.split("T")[0];
            var BMI = izracunajBMI(heights[i].height, weights[i].weight);
             $("#tBody").append("<tr onClick=\"showElement(" + i + ")\" id=\"master" + i + "\"><td>" + date + " " + time +"</td><td>" + weights[i].weight +"</td><td>" + heights[i].height +"</td><td>" +  BMI + "</td><td>" + izpisiBMI(BMI) + "</td></tr>");
           
           
            if (i < heights.length - 1) {
                var razlikaBMI = (BMI - izracunajBMI(heights[i + 1].height, weights[i + 1].weight)).toFixed(2);
                var razlikaTeza = weights[i].weight - weights[i + 1].weight;
            
            
            if (razlikaTeza < 0) {
                razlikaBMI = Math.abs(razlikaBMI);
                razlikaTeza = Math.abs(razlikaTeza);
                if (BMI < 18.5) {
                    $("#tBody").append("<tr id=\"detail" + i + "\" hidden style=\"background-color:#ff9999\"><td style=\"padding-left:5%; padding-bottom:3%\">Izguba teže: </td><td>" + razlikaTeza + "</td><td></td><td>" + razlikaBMI + "</td><td></td></tr>");
                    } 
                    else if (BMI < 24.9) $("#tBody").append("<tr id=\"detail" + i + "\" hidden style=\"background-color:#d8d8d8\"><td style=\"padding-left:5%; padding-bottom:3%\">Izguba teže: </td><td>" + razlikaTeza + "</td><td></td><td>" + razlikaBMI + "</td><td></td></tr>");
                    else $("#tBody").append("<tr id=\"detail" + i + "\" hidden style=\"background-color:#b3ffc3\"><td style=\"padding-left:5%; padding-bottom:3%\">Izguba teže: </td><td>" + razlikaTeza + "</td><td></td><td>" + razlikaBMI + "</td><td></td></tr>");
                }
            else if (razlikaTeza == 0) $("#tBody").append("<tr id=\"detail" + i + "\" hidden style=\"background-color:#d8d8d8\"><td style=\"padding-left:5%; padding-bottom:3%\">Ni razlike: </td><td>" + razlikaTeza + "</td><td></td><td>" + razlikaBMI + "</td><td></td></tr>");
            else {
                if (BMI < 18.5)
               $("#tBody").append("<tr id=\"detail" + i + "\" hidden style=\"background-color:#b3ffc3\"><td style=\"padding-left:5%; padding-bottom:3%\">Pridobitev na teži: </td><td>" + razlikaTeza + "</td><td></td><td>" + razlikaBMI + "</td><td></td></tr>"); 
               else if (BMI < 24.9) $("#tBody").append("<tr id=\"detail" + i + "\" hidden style=\"background-color:#d8d8d8\"><td style=\"padding-left:5%; padding-bottom:3%\">Pridobitev na teži: </td><td>" + razlikaTeza + "</td><td></td><td>" + razlikaBMI + "</td><td></td></tr>"); 
               else $("#tBody").append("<tr id=\"detail" + i + "\" hidden style=\"background-color:#ff9999\"><td style=\"padding-left:5%; padding-bottom:3%\">Pridobitev na teži: </td><td>" + razlikaTeza + "</td><td></td><td>" + razlikaBMI + "</td><td></td></tr>"); 
            } 
              }
            }
    $("#tabela").append("</tbody></table>");
    }
}


function prikaziPodatke() {
    var poizvedbaEhr = "";
    $(".chart").children().remove();
    $("#sporociloNapak").children().remove();
     $("#sporocilo").children().remove();
     $("#sporocilo").hide();
     $("#fitness").hide();
    if ($("#izbiraOsebe").val() > 0) {
        poizvedbaEhr = $("#vnosEhr").val();
        getData(poizvedbaEhr, function(weights, heights, party) {
            var age = getAge(party.dateOfBirth);
            if (weights.length > 0) {
             drawChart(weights, heights, party.gender, age);
                $(".chart").append("<div id=\"chart_div\" style=\"width: 900px; height: 500px;\"></div>");
                if (izracunajBMI(heights[heights.length-1].height, weights[weights.length-1].weight) < 18.5) {
                    lokacija("restaurant");
                    $("#sporocilo").append("<h3>Ste presuhi, predlagamo, da kaj pojeste.</h3>");
                     $("#sporocilo").css('background-color', '#ff4d4d');
                     $("#textFitness").text("Najbližje restavracije");
                   
                }
                else if(izracunajBMI(heights[0].height, weights[0].weight) < 24.9) {
                     $("#sporocilo").append("<h3>Vaš trenutni BMI je ravno pravšenj.</h3>");
                     $("#sporocilo").css('background-color', '#4ca565');
                }
                else {
                    $("#sporocilo").append("<h3>Imate prekomerno telesno težo, predlagamo, da greste v fitnes.</h3>");
                    lokacija("fitness");
                     $("#sporocilo").css('background-color', '#ff4d4d');
                     $("#textFitness").text("Najbližji fitnesi");
                } 
                $("#sporocilo").show();
                
            } else $("#sporociloNapak").html("<span class='obvestilo " +
      "label label-warning fade-in'>Ni podatkov</span>");
            drawTable(weights, heights, party.gender, age);

        });
    }


}

function getAge(dateOfBirth) {
    return new Date().getFullYear() - dateOfBirth.split("-")[0];
}
    
    function drawChart(weights, heights, gender, age) {
          google.charts.load('current', {'packages':['corechart']});
          google.charts.setOnLoadCallback(drawVisualization);
            
          function drawVisualization() {
            // Some raw data (not necessarily accurate)
            
            var data = [['Month', 'Telesna teža', 'Telesna višina',  'BMI']];
            for (var i = heights.length - 1; i >= 0; i--) {
                 var date = heights[i].time.split("T")[0];
                 var BMI = parseFloat(izracunajBMI(heights[i].height, weights[i].weight));
                 var height = parseFloat(heights[i].height);
                 var weight = parseFloat(weights[i].weight);
                 data.push([date, weight, height, BMI]);
                }
            data = google.visualization.arrayToDataTable(data);
    
        var options = {
          title : 'Vaše stanje',
          vAxis: {title: 'Višina / teža'},
          seriesType: 'bars',
          series: {2: {type: 'line', targetAxisIndex:1}},
            
          vAxes: {
            0: {title: 'Višina (cm) / teža (kg)'},
            1: {
              title:'BMI',
            }
          }
        };
    
        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    }

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija


function kreirajPacienta(ime, priimek, datumRojstva, spol, ehrId) {
    demoPodatki.push({name: ime, lastName:priimek, dateOfBirth:datumRojstva, gender:spol, ehrId:ehrId});
}

window.addEventListener('load', function() {
   sessionId = getSessionId();
	
 
    $('#preset').change(updateGenerateView);
  
  
  $('#izbiraOsebe').change(function() {
    var i = $(this).val() - 1;
     if (i > -1) $("#vnosEhr").val(demoPodatki[i].ehrId);
     
  });
  
  $('#vnosIzbiraOsebe').change(function() {
      var i = $(this).val() - 1;
     if (i > -1) $("#vasEhrId").val(demoPodatki[i].ehrId);
  });
  
       for (var i = 0; i < demoPodatki.length; i++) {
        $("#izbiraOsebe").append("<option value=\"" + (i+1) + "\">" + demoPodatki[i].name + " " + demoPodatki[i].lastName +"</option>");
        $("#vnosIzbiraOsebe").append("<option value=\"" + (i+1) + "\">" + demoPodatki[i].name + " " + demoPodatki[i].lastName +"</option>");
   }
   
   
});

function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();
	
	var year = new Date().getYear() + 1900 + "";
	var month = (new Date().getMonth() + 1) + "";
	var day = new Date().getDate() + "";
	var hours = new Date().getHours();
	var minutes = new Date().getMinutes();
	if (day < 10) day = "0" + day;
	if (month < 10) month = "0" + month;
	if (hours < 10) hours = "0" + hours;
	if (minutes < 10) minutes = "0" + minutes;
	
    
	var ehrId = $("#vasEhrId").val();
	var datumInUra = year + "-" + month + "-" + day + "T" + hours + ":" + minutes + "Z";
	var telesnaVisina = $("#telesnaVisina").val();
	var telesnaTeza = $("#telesnaTeza").val();

	if (!ehrId || ehrId.trim().length == 0 || !telesnaTeza || !telesnaVisina) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
            // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}

function izracunajBMI(height, weight) {
    return (weight / height / height * 10000).toFixed(2);
}

function izpisiBMI(BMI) {
    if (BMI < 18.5) return "Prenizka telesna masa";
    else if(BMI < 24.9) return "Normalna telesna masa";
    else if(BMI < 29.9) return "Prekomerna telesna masa";
    else return "Debelost";
}

function showElement(i) {
     $('#detail' + i).toggle();
}

function lokacija(type) {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            initMap(position, type);
        });
    } else {
        console.log("Geolocation is not supported by this browser.");
    }
}




      var map;
      var infowindow;

      function initMap(myLocation, type) {
           $("#fitness").show();
        var koordinate = {lat:myLocation.coords.latitude, lng:myLocation.coords.longitude}
        map = new google.maps.Map(document.getElementById('map'), {
          center: koordinate,
          zoom: 11
        });
        
         var request = {
            location: koordinate,
            radius: '15000',
            query: type
          };

        infowindow = new google.maps.InfoWindow();
        var service = new google.maps.places.PlacesService(map);
        service.textSearch(request, callback);
      }

      function callback(results, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
          for (var i = 0; i < results.length; i++) {
            createMarker(results[i]);
          }
        }
      }

      function createMarker(place) {
        var placeLoc = place.geometry.location;
        var marker = new google.maps.Marker({
          map: map,
          position: place.geometry.location
        });

        google.maps.event.addListener(marker, 'click', function() {
          infowindow.setContent(place.name);
          infowindow.open(map, this);
        });
      }
      
function updateGenerateView() {
        var i = $("#preset").val() - 1;
            if (i > -1) {
            $("#generateName").val(demoPodatki[i].name);
            $("#generateLastName").val(demoPodatki[i].lastName);
            $("#generateDateOfBirth").val(demoPodatki[i].dateOfBirth);
        if (demoPodatki[i].gender == "male") $("#izberiSpol").val("male");
        else $("#izberiSpol").val("female");
    }
}
